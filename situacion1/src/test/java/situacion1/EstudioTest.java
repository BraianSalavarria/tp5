package situacion1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import situacion1.Codigo.Cliente;
import situacion1.Codigo.Domicilio;
import situacion1.Codigo.Estudio;
import situacion1.Excepciones.ClienteInexistenteException;
import situacion1.Excepciones.ClienteExistenteException;

public class EstudioTest{

private Cliente cliente;
private Cliente cliente2;
private Estudio estudio;
private Domicilio domicilio;



@Before
    public void setUp(){
         cliente =  new Cliente("Braian","Salavarria",38915480, domicilio ,"braian.salavarria@gmail.com","tramitando dpro","3834766228");
         cliente2 = new Cliente("Carlos","Salavarria",38915479,domicilio,"carlos.salavarria@gmail.com","tramitanto dpto","3834765652");
        estudio = new Estudio();
    }

    @Test
        public void comprobarNuevoEstudio(){
            
            Estudio estudioNuevo = new Estudio();
            assertEquals(0, estudioNuevo.getClientes().size());
        }

    @Test 
        public void agregarClieteAlEstudio() throws ClienteExistenteException {
            
            estudio.agregarCliente(cliente);
            assertEquals(1, estudio.getClientes().size());      
    }

    @Test
        public void eliminarClienteDelEstudio() throws ClienteExistenteException, ClienteInexistenteException {
            estudio.agregarCliente(cliente);
            estudio.agregarCliente(cliente2);
            estudio.eliminarCliente(38915480);
            assertEquals(1, estudio.getClientes().size());
        }


    @Test
        public void obtenerClienteDelEstudio() throws ClienteExistenteException, ClienteInexistenteException {
            estudio.agregarCliente(cliente);
            estudio.agregarCliente(cliente2);
            Cliente ObjetoEsperado = estudio.getCliente(38915480);
            
            assertEquals(cliente, ObjetoEsperado);
        }


        @Test
            public void modificarClienteDelEstudio() throws ClienteExistenteException, ClienteInexistenteException {
                Cliente nuevoCliente= new Cliente ("Braian","Gonzales",38915480, domicilio ,"braian.salavarria@gmail.com","tramitando dpro","3834766228");
                estudio.agregarCliente(cliente);
                estudio.modificarCliente(nuevoCliente);
                
                assertEquals("Gonzales", estudio.getCliente(38915480).getApellido());
            }

//--------------------------------------TESTEO DE LAS EXCEPCIONES------------------------------------------------


        @Test (expected = ClienteExistenteException.class)
            public void agregarClienteExistenteAlEstudio() throws ClienteExistenteException {
                  
                estudio.agregarCliente(cliente);
                estudio.agregarCliente(cliente);
            }

        
        @Test (expected = ClienteInexistenteException.class)
            public void buscarClienteInexistenteEnElEstudio() throws ClienteExistenteException,
                    ClienteInexistenteException {
                estudio.agregarCliente(cliente);
                estudio.getCliente(38915485);
            }




}
