
package situacion1.ModeloDeTabla;

import situacion1.Codigo.Cliente;
import situacion1.Codigo.Estudio;
import situacion1.Excepciones.ClienteInexistenteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Braian
 */
public class ModeloTabla extends DefaultTableModel {
    
    String columnas[];
    static Integer NOMBRE=0; 
    static Integer APELLIDO=1;
    static Integer DNI=2;
    //static Integer DOMICILIO=3;
    private Estudio estudio;
   

    public ModeloTabla(String columnas[]) {
        super();
        this.columnas = columnas;
        this.setColumnIdentifiers(columnas);
    }

    @Override
    public boolean isCellEditable(int row, int column) {

        if (column == 3) {
            return true;
        } else {
            return false;
        }

    }

    public void cargarDatoAlaTabla(Estudio estudio) {

    
        this.estudio = estudio;
        Object fila[] = new Object[3];
        
        for(int i =0; i<estudio.getClientes().size();i++){
            
            fila[NOMBRE]=estudio.getClientes().get(i).getNombre();
            fila[APELLIDO]=estudio.getClientes().get(i).getApellido();
            fila[DNI]=estudio.getClientes().get(i).getDni();
           // fila[DOMICILIO]=estudio.getClientes().get(i).getDomicilio();
            
        }
        this.addRow(fila);
    }

    public void cargarDatosAlaTabla(Estudio estudio){
        
        Object fila[] = new Object[4];
        
        for(int i =0; i<estudio.getClientes().size();i++){
            
            fila[NOMBRE]=estudio.getClientes().get(i).getNombre();
            fila[APELLIDO]=estudio.getClientes().get(i).getApellido();
            fila[DNI]=estudio.getClientes().get(i).getDni();
          //  fila[DOMICILIO]=estudio.getClientes().get(i).getDomicilio().mostrarInformacion();
            this.addRow(fila);
        }
 }


public void EliminarElementoLaTabla(JTable tabla,Estudio estudio){
    Integer fila;
    
    fila = tabla.getSelectedRow();
    String datoDeFila = String.valueOf(this.getValueAt(fila, DNI));
    Integer dni = Integer.parseInt(datoDeFila);
    
    if(fila>=0){
        try {
            this.removeRow(fila);
            estudio.eliminarCliente(dni);
        } catch (ClienteInexistenteException ex) {
            Logger.getLogger(ModeloTabla.class.getName()).log(Level.SEVERE, null, ex);
        }
        }else{       
            JOptionPane.showMessageDialog(null,"Por Favor Seleccione un Producto");  
        }               
}


 public void cargarDatoAlaTabla(Cliente cliente, JTable tabla){
     tabla.removeAll();
     Object [] fila = new Object [4];
     fila[NOMBRE] =cliente.getNombre();
     fila[APELLIDO]=cliente.getApellido();
     fila[DNI]=cliente.getDni();
     this.addRow(fila);
    
 }













}

