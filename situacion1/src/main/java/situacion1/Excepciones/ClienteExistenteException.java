package situacion1.Excepciones;

public class ClienteExistenteException extends Exception{
    
    public ClienteExistenteException(){
        super("El Cliente ya Existe");
    }
}