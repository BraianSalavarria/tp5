package situacion1.Excepciones;

public class ClienteInexistenteException extends Exception{
    
    public ClienteInexistenteException(){
        super("El Cliente no Existe");
    }
}