package situacion1.Codigo;

public class Barrial extends Domicilio {

 private String nombre;
 private String manzana;
 private Integer numeroCasa;

    public Barrial(String nombre, String manzana, Integer numeroCasa) {
        this.setNombre(nombre);
        this.setManzana(manzana);
        this.setNumeroCasa(numeroCasa);
    }

public String nombre() {
    return nombre;
}
public void setNombre(String nombre) {
    this.nombre = nombre;
}

public String getManzana(){
    return manzana;
}
public void setManzana(String manzana) {
    this.manzana = manzana;
}

public Integer getNumeroCasa() {
    return numeroCasa;
}
public void setNumeroCasa(Integer numeroCasa) {
    this.numeroCasa = numeroCasa;
}

@Override
public String mostrarInformacion() {
    return "NOMBRE : " + nombre +"\n"+ "MANZANA : " + manzana +"\n"+"NUMERO DE CASA : " + numeroCasa;

}



}