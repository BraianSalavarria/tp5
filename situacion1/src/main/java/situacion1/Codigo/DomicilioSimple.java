package situacion1.Codigo;

public  class DomicilioSimple extends Domicilio{
    
    private String calle;
    private Integer numero;

    public DomicilioSimple(String calle, Integer numero){
        this.setCalle(calle);
        this.setNumero(numero);
    }

    public String getCalle() {
        return calle;
    }
    public void setCalle(String calle) {
        this.calle = calle;
    }

    public Integer getNumero() {
        return numero;
    }
    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    @Override
    public String mostrarInformacion() {
        return "CALLE : "+ calle +"\n" +"NUMERO :" + numero;
    }



}