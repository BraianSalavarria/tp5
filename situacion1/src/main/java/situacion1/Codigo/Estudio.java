package situacion1.Codigo;

import java.util.ArrayList;
import java.util.Collections;

import situacion1.Excepciones.ClienteExistenteException;
import situacion1.Excepciones.ClienteInexistenteException;
import situacion1.OrdenDeArray.OrdenaPorDni;
import situacion1.OrdenDeArray.OrdenaPorApellido;


public class Estudio {
  
    private ArrayList<Cliente> clientes;
	
    public Estudio(){
        this.setClientes(new ArrayList<Cliente>());
    }

    
    public ArrayList<Cliente> getClientes(){
        return clientes;
    }
    public void setClientes(ArrayList<Cliente> clientes){
        this.clientes = clientes;
    }

    public void agregarCliente(Cliente cliente) throws ClienteExistenteException {
        for (Cliente var : clientes) {
            if (var.getDni().equals(cliente.getDni())){
                throw new ClienteExistenteException();
            }
        }
        clientes.add(cliente);
    }

    public void eliminarCliente(Integer dni) throws ClienteInexistenteException {
        Cliente clienteEncontrado = getCliente(dni);
        clientes.remove(clienteEncontrado);
    }

    public Cliente getCliente(Integer dni) throws ClienteInexistenteException{
        Cliente clienteEncontrado = null;
            
            for (Cliente var : clientes) {
                if(var.getDni().equals(dni)) {
                    clienteEncontrado = var;
                }
            }
            if (clienteEncontrado == null) {
                throw new ClienteInexistenteException();
            }
            return clienteEncontrado; 
        }
         
        
     public void modificarCliente(Cliente nuevoCliente) throws ClienteInexistenteException {
        Cliente clienteEncontrado = null;
        clienteEncontrado = getCliente(nuevoCliente.getDni());
        clientes.remove(clienteEncontrado);
        clientes.add(nuevoCliente);
     }   

     
     public void OrdenarPorApellido(){
          
         Collections.sort(clientes, new OrdenaPorApellido());             
     }
     
     public void OrdenarPorDni(){
         
         Collections.sort(clientes, new OrdenaPorDni());
     }
     
        
     
        public void mostrarDatos(){
            for (Cliente var : clientes) {
                System.out.println("--------------------Datos del Cliente--------------------------");
                System.out.println(var.toString());
                System.out.println("--------------------Datos del Domicilio------------------------");
                System.out.println(var.getDomicilio().mostrarInformacion());
            }
        }
        
    }
    