package situacion1.Codigo;

public class Cliente implements Comparable<Cliente> {
    
private String nombre;
private String apellido;
private Integer dni;
private Domicilio domicilio;
private String correoElectronico;
private String asunto;
private String numeroTelefonico;

    public Cliente(String nombre,String apellido,Integer dni , Domicilio domicilio ,String correoElectronico,String asunto, String numeroTelefonico){
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setDni(dni);
        this.setDomicilio(domicilio);
        this.setCorreoElectronico(correoElectronico);
        this.setAsunto(asunto);
        this.setNumeroTelefonico(numeroTelefonico);
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getDni() {
        return dni;
    }
    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }
    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }
    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getAsunto() {
        return asunto;
    }
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String numeroTelefonico(){
        return numeroTelefonico;
    }
    public void setNumeroTelefonico(String numeroTelefonico) {
        this.numeroTelefonico = numeroTelefonico;
    }

    @Override 
    public String toString(){
        return "Nombre: "+nombre+"\n"+"Apellido: "+apellido+"\n"+"Dni :"+dni+"\n"+"Correo Electronico: "+correoElectronico+"\n"+"Numero Telefonico :"+numeroTelefonico+"\n"+"Asunto: "+asunto;
        

    }

    @Override
    public int compareTo(Cliente o) {
        
        return  dni.compareTo(o.getDni());
    }
}

