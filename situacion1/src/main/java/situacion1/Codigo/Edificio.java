package situacion1.Codigo;

public class Edificio extends Domicilio{
    
private String calle;
private Integer  piso;
private String departamento;

public Edificio(String calle, Integer piso,String departamento) {
    this.setCalle(calle);
    this.setPiso(piso);
    this.setDepartamento(departamento);
}

public String getCalle(){
    return calle;
}
public void setCalle(String calle) {
    this.calle = calle;
}

public Integer getPiso() {
    return piso;
}
public void setPiso(Integer piso) {
    this.piso = piso;
}

public String getDeparmento() {
    return departamento;
}
public void setDepartamento(String departamento) {
    this.departamento = departamento;
}

@Override
public String mostrarInformacion() {
    return "CALLE : " + calle +"\n"+ "PISO : " + piso +"\n"+"DEPARTAMENTO : " + departamento;

}

}