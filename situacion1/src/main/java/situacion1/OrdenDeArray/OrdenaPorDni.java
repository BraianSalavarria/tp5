
package situacion1.OrdenDeArray;

import java.util.Comparator;
import situacion1.Codigo.Cliente;

/**
 *
 * @author Braian
 */
public class OrdenaPorDni implements Comparator{

    @Override
    public int compare(Object o1, Object o2) {
        Cliente cliente1 = (Cliente)o1;
        Cliente cliente2 = (Cliente)o2;
        return cliente1.getDni().compareTo(cliente2.getDni());
    }
    
}
