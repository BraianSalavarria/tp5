
package situacion2.OrdenarArrays;

import situacion2.codigo.Item;
import java.util.Comparator;

/**
 *
 * @author Braian
 */
public class OrdenaPorPrecio implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Item producto1 =(Item)o1;
        Item producto2 =(Item)o2;
      
        return producto1.getprecioUnitario().compareTo(producto2.getprecioUnitario());
    }
    
}
