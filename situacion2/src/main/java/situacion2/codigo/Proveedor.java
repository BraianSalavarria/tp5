package situacion2.codigo;
public class Proveedor {
    
    private String nombre;
    private String apellido;
    private Integer dni; 
    private String domicilio;


    public Proveedor(String nombre, String apellido, Integer dni, String domicilio){
        
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setDni(dni);
        this.setDomicilio(domicilio);

    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    @Override
    public String toString() {
        return "Apellido: " + apellido +"\nNombre: "+nombre+"\nDNI: " + dni + "\nDomicilio: " + domicilio;
    }

    

    
}
