package situacion2.codigo;
import java.math.BigDecimal;

public class MateriaPrima {

    private String nombre;
    private BigDecimal precio; 
    private Proveedor proveedor; 


    public MateriaPrima(String nombre, BigDecimal precio, Proveedor proveedor){

        this.setNombre(nombre);
        this.setPrecio(precio);
        this.setProveedor(proveedor);

    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    


    
}
