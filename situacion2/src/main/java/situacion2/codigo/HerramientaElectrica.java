package situacion2.codigo;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class HerramientaElectrica extends Herramienta {

    private String funcionalidad;

    public HerramientaElectrica(String nombre, Integer codigo, BigDecimal precio, String funcionalidad, BigDecimal cantidad) {
        super(nombre, codigo, precio,cantidad);
        this.setFuncionalidad(funcionalidad);
    }

    public String getFuncionalidad() {
        return funcionalidad;
    }
    
    public void setFuncionalidad(String funcionalidad) {
        this.funcionalidad = funcionalidad;
    }
// **************************************************IMPLEMENTANDO METODOS******************************************
   
    @Override
    public String getNombreProducto() {
        return super.getNombre();
        
    }

    @Override
    public Integer codigoProducto() {
        return super.getCodigo();
    }

    @Override
    public BigDecimal getprecioUnitario() {
        return super.getPrecio();
    }

    @Override
    public BigDecimal getPrecioDeVenta() {
        BigDecimal monto = new BigDecimal("0.00");
        monto = monto.add(super.getPrecio().multiply(super.getCantidad()));
        return monto.setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getCantidadArticulos() {
       return super.getCantidad();
    }

    
    
    
    
}
