package situacion2.codigo;
import situacion2.Excepciones.ProductoExistenteException;
import situacion2.OrdenarArrays.OrdenaPorNombre;
import situacion2.OrdenarArrays.OrdenaPorPrecio;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class OrdenVenta {


    private ArrayList<Item> items;
    private LocalDate fechaDeEmicion;
    private Integer nroOrden;
    
    
    public OrdenVenta () {

        this.setItems(new ArrayList<Item>());
    }
    

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }
    public ArrayList<Item> getItems() {
        return items;
    }

    public void agregarProducto(Item producto) throws ProductoExistenteException{
        for (Item var : items) {
            if( var.codigoProducto().equals(producto.codigoProducto())){
                throw new ProductoExistenteException();
            }
       }
        items.add(producto);   
    }
  
    public Integer getsize(){
        return items.size();
    }
    
    
    
   public BigDecimal montoTotal(){
       
       BigDecimal total = new BigDecimal("0.00");
       for(Item var : items ) {
           total = total.add(var.getPrecioDeVenta());
       }
       return total.setScale(2, RoundingMode.HALF_UP); 
   }
   
    
public void ordenarPorNombre(){
   
         Collections.sort(items, new OrdenaPorNombre());              
 }

public void ordenarPorPrecio(){
        
        Collections.sort(items, new OrdenaPorPrecio());   
}
   
   
   
   
   
   
   
   
   


}
