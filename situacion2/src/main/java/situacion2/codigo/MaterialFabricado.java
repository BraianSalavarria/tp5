package situacion2.codigo;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class MaterialFabricado implements Item {
    

    private Integer codigo; 
    private String nombre; 
    private BigDecimal precio; 
    private BigDecimal cantidad;

    public MaterialFabricado(String nombre, Integer codigo, BigDecimal precio, BigDecimal cantidad){

        this.setNombre(nombre);
        this.setCodigo(codigo);
        this.setPrecio(precio);
        this.setCantidad(cantidad);
    }



    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public String getNombre(){
        return nombre;
    }

    public void setCodigo(Integer codigo){
        this.codigo = codigo;
    }
    public Integer getCodigo(){
        return codigo;
    }

    public void setPrecio(BigDecimal precio){
        this.precio = precio;
    }
    public BigDecimal getPrecio(){
        return precio; 
    }
    public void setCantidad(BigDecimal cantidad){
        this.cantidad = cantidad;
    }
    public BigDecimal getCantidad(){
        return cantidad;
    }

    @Override
    public String getNombreProducto() {
        return getNombre();
    }

    @Override
    public Integer codigoProducto() {
        return getCodigo();
    }

    @Override
    public BigDecimal getprecioUnitario() {
        return getPrecio();
    }

    @Override
    public BigDecimal getPrecioDeVenta() {
        BigDecimal monto = new BigDecimal("0.00");
        monto = monto.add(getPrecio().multiply(getCantidad()));
        return monto.setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getCantidadArticulos() {
       return getCantidad();
    }

   




}
