package situacion2.codigo;
import java.math.BigDecimal;

public abstract class Herramienta implements Item {
    

    private Integer codigo;
    private String nombre;
    private BigDecimal precio;
    private BigDecimal cantidad;



    public Herramienta(String nombre, Integer codigo, BigDecimal precio ,BigDecimal cantidad){
        this.setNombre(nombre);
        this.setCodigo(codigo);
        this.setPrecio(precio);
        this.setCantidad(cantidad);
    }


    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public BigDecimal getCantidad(){
        return cantidad;
    }
    
    public void setCantidad(BigDecimal cantidad){
        this.cantidad = cantidad;
    }    
    
}
