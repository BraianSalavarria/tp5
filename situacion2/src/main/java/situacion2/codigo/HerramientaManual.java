package situacion2.codigo;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class HerramientaManual extends Herramienta {

    public HerramientaManual(String nombre, Integer codigo, BigDecimal precio ,BigDecimal cantidad) {
        super(nombre, codigo, precio, cantidad);
        
    }

    @Override
    public String getNombreProducto() {
        return super.getNombre();
    }

    @Override
    public Integer codigoProducto() {
       return super.getCodigo();
    }

    @Override
    public BigDecimal getprecioUnitario() {
         return super.getPrecio();
    }

    @Override
    public BigDecimal getPrecioDeVenta() {
        BigDecimal monto = new BigDecimal("0.00");
        monto = monto.add(super.getPrecio().multiply(super.getCantidad()));
        return monto.setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getCantidadArticulos() {
       return super.getCantidad();
    }

    




}
